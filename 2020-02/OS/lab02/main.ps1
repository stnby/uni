New-Item -Path HKCU:\Software\PowershellScriptRunTime -Value $(Get-Date -format "yyyy-MM-dd HH:mm") -Force | Out-Null

Switch ($args[0]) {
    "kill" {
        Try {
            Switch ($args[1]) {
                "-Id" {
                    "Searching for processes by Id"
                    $Processes = Get-Process -Id $args[2] -ErrorAction Stop
                    break
                }
                "-Name" {
                    "Searching for processes by Name"
                    $Processes = Get-Process -Name $args[2] -ErrorAction Stop
                    break
                }
                default {
                    "Available options -Id, -Name"
                    return
                }
            }
        }
        Catch {
            "Could not find the process"
            return
        }
        $Processes | ForEach-Object -Process {
            $_
            if ((Read-Host "Do you want to kill this process [Y/n]") -eq 'Y') {
                "Killing the process..."
                Stop-Process -InputObject $_ -Force
            }
        }
        break
    }
    "log" {
        While ($true) {
            $CurrentTime = Get-Date -format "yyyy-MM-dd_HH_mm_ss"
            $FilePath = ".\logs\FilteredProcessList" + $CurrentTime + ".csv"
            "Writing process list to a file..."
            Get-Process | Sort ws -Descending | Select Name, Id, WS | Export-Csv $FilePath -NoTypeInformation
            gci .\logs -Recurse | where{-not $_.PsIsContainer} | sort CreationTime -desc | select -Skip 5 | Remove-Item -Force
            Start-Sleep -Seconds 5
        }
        break
    }
    default {
        "Available comands: kill, log"
    }
}
