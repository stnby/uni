#include <stdio.h>

/*
 * File format:
 * 500
 * 200
 * 100
 * 50
 * 20
 */

const char fileName[] = "data.txt";

void readBalance(int *balance) {
    FILE* fp = fopen(fileName, "r");
    for (int i = 0; i < 5; i++)
        fscanf(fp, "%d", &balance[i]);
    fclose(fp);
}

void writeBalance(int *balance) {
    FILE* fp = fopen(fileName, "w");
    for (int i = 0; i < 5; i++)
        fprintf(fp, "%d\n", balance[i]);
    fclose(fp);
}

void printStatus(int *notes) {
    int note;
    for (int i = 0; i < 5; i++) {
        switch(i) {
            case 0:
                note = 500;
                break;
            case 1:
                note = 200;
                break;
            case 2:
                note = 100;
                break;
            case 3:
                note = 50;
                break;
            default:
                note = 20;
        }
        printf("%dx %d€\n", notes[i], note);
    }
}

void withdrawMoney(int *balance) {
    int amount, withdraw[5] = {0};
    printf("Amount to withdraw: ");
    scanf("%d", &amount);

    while (amount > 0) {
        if (balance[0] > 0 && amount - 500 >= 0) {
            amount -= 500;
            withdraw[0]++;
        }
        else if (balance[1] > 0 && amount - 200 >= 0) {
            amount -= 200;
            withdraw[1]++;
        }
        else if (balance[2] > 0 && amount - 100 >= 0) {
            amount -= 100;
            withdraw[2]++;
        }
        else if (balance[3] > 0 && amount - 50 >= 0) {
            amount -= 50;
            withdraw[3]++;
        }
        else if (balance[4] > 0 && amount - 20 >= 0) {
            amount -= 20;
            withdraw[4]++;
        }
        else {
            puts("Can not withdraw this amount!");
            return;
        }
    }
    for (int i = 0; i < 5; i++)
        balance[i] -= withdraw[i];
    printStatus(withdraw);
    //writeBalance(balance);
}

int main() {
    int balance[5];
    readBalance(balance);
    int opt;
    do {
        printf("1. Withdraw money.\n2. Show ATM statistics.\n3. Quit.\nChoice: ");
        scanf("%d", &opt);
        switch (opt) {
            case 1:
                withdrawMoney(balance);
                break;
            case 2:
                printStatus(balance);
                break;
            case 3:
                break;
            default:
                puts("Invalid option!");
        }
    } while (opt != 3);
    writeBalance(balance);
    return 0;
}