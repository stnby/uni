#include <stdio.h>

static char* weekDays[] = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
static int knownMonday = 1990;

int isLeap(int year) {
    if (year % 4 == 0)
        return 1;
    return 0;
}

int diffFromKnownMonday(year) {
    int diff = 0;
    if (knownMonday < year)
        for (;knownMonday < year--; diff += isLeap(year) ? 366 : 365);
    else if (knownMonday > year)
        for (;knownMonday > year++; diff += isLeap(year) ? 366 : 365);
    return diff;
}

int main() {
    int year;
    printf("Enter your year: ");
    scanf("%d", &year);
    int diff = diffFromKnownMonday(year);
    printf("Leap-year: %s\n", isLeap(year) ? "yes" : "no");
    printf("First day of the year: %s\n", weekDays[diff % 7]);
    printf("Last day of the year: %s\n", weekDays[(diff + isLeap(year)) % 7]);
    int weekends = 0;
    for (int i = diff + (isLeap(year) ? 365 : 364); i > diff; i--) {
        if (i % 7 == 5 || i % 7 == 6)
            weekends++;
    }
    printf("Amount of wekends in a year: %d\n", weekends);
    return 0;
}