#include <iostream>
#include <strings.h>

using namespace std;

struct bidder {
	string name;
	float bid;
	bidder *next;
};

struct painting {
	int id;
	float price;
	float earnings = 0;
	bidder *bidders;
	painting *next;
};

painting *first = NULL;

bool checkPaintingId(int id) {
	painting *currPainting = first;
	while (currPainting != NULL) {
		if (currPainting->id == id) {
			return 1;
		}
		currPainting = currPainting->next;
	}
	return 0;
}

void registerPainting() {
	int id;
	cout << "Enter the painting id: ";
	cin >> id;
	if (checkPaintingId(id)) {
		cout << "Painting with this id already exists.\n";
		return;
	}
	painting *newPainting = new painting;
	painting *currPainting = first;
	painting *prevPainting;
	cout << "Enter the painting price: ";
	cin >> newPainting->price;
	if (newPainting->price <= 0) {
		cout << "Price can not be negative or zero.\n";
		return;
	}
	newPainting->id = id;
	while (currPainting != NULL && newPainting->price < currPainting->price) {
		prevPainting = currPainting;
		currPainting = currPainting->next;
	}
	if (prevPainting == NULL)
		first = newPainting;
	else
		prevPainting->next = newPainting;
	newPainting->next = currPainting;
}

void registerBidder() {
	painting *currPainting = first;
	int id;
	cout << "Enter the painting id: ";
	cin >> id;
	while (currPainting != NULL) {
		if (currPainting->id == id) {
			bidder *newBidder = new bidder;
			bidder *currBidder = currPainting->bidders;
			bidder *prevBidder;
			cout << "Enter your name: ";
			cin.ignore();
			getline(cin, newBidder->name);
			cout << "Enter your bid: ";
			cin >> newBidder->bid;
			if (newBidder->bid < currPainting->price) {
				cout << "Bid can not be lower than the price of the painting.\n";
				return;
			}
			while (currBidder != NULL && newBidder->bid < currBidder->bid) {
				prevBidder = currBidder;
				currBidder = currBidder->next;
			}
			if (prevBidder == NULL)
				currPainting->bidders = newBidder;
			else
				prevBidder->next = newBidder;
			newBidder->next = currBidder;
			return;
		}
		currPainting = currPainting->next;
	}
	cout << "Painting with this id does not exist.\n";
}

void listPaintings() {
	cout << "Paintings:\n";
	painting *currPainting = first;
	while (currPainting != NULL) {
		cout << currPainting->id << ". " << currPainting->price << "Eur.\n";
		currPainting = currPainting->next;
	}
}

void registerPurchase() {
	painting *currPainting = first;
	int id;
	cout << "Enter the painting id: ";
	cin >> id;
	while (currPainting != NULL) {
		if (currPainting->id == id) {
			if (currPainting->bidders != NULL) {
				bidder *currBidder = currPainting->bidders;
				currPainting->earnings += currBidder->bid;
				cout << "Painting was sold to " << currBidder->name << " for " << currBidder->bid << "Eur.\n";
				currPainting->bidders = currBidder->next;
				delete currBidder;
			}
			else
				cout << "Painting does not have any bidders.\n";
			return;
		}
		currPainting = currPainting->next;
	}
	cout << "Painting with this id does not exist.\n";
}

void listBidders() {
	painting *currPainting = first;
	int id;
	cout << "Enter the painting id: ";
	cin >> id;
	cout << "Bidders:\n";
	while (currPainting != NULL) {
		if (currPainting->id == id) {
			bidder *currBidder = currPainting->bidders;
			while (currBidder != NULL) {
				cout << currBidder->name << " " << currBidder->bid << "Eur.\n";
				currBidder = currBidder->next;
			}
			return;
		}
		currPainting = currPainting->next;
	}
	cout << "Painting with this id does not exist.\n";
}

void removeUnwanted() {
    painting *currPainting = first;
	painting *prevPainting;
	while (currPainting != NULL) {
		prevPainting = currPainting;
		if (currPainting->bidders == NULL) {
			if (currPainting == first)
				first = first->next;
			else
				prevPainting->next = currPainting->next;
			delete currPainting;
		}
		currPainting = currPainting->next;
    }
}

void printEarnings() {
	painting *currPainting = first;
	cout << "Earnings:\n";
	while (currPainting != NULL) {
		cout << currPainting->id << ". " << currPainting->earnings << "Eur.\n";
		currPainting = currPainting->next;
	}
}

void printPopular() {
	if (first != NULL) {
		painting *currPainting = first;
		int popularId, popularBidderCount = 0;
		while (currPainting != NULL) {
			bidder *currBidder = currPainting->bidders;
			int currBidderCount = 0;
			while (currBidder != NULL) {
				currBidderCount++;
				currBidder = currBidder->next;
			}
			if (currBidderCount > popularBidderCount) {
				popularId = currPainting->id;
				popularBidderCount = currBidderCount;
			}
			currPainting = currPainting->next;
		}
		if (popularBidderCount > 0)
			cout << popularId << " is the most polpular painting with " << popularBidderCount << " bidders.\n";
		else
			cout << "There are no paintings with bidders.\n";
	}
	else
		cout << "There are no paintings.\n";
}

int main() {
	int opt;
	do {
		cout << "\n1. Register painting.\n2. Register bidder.\n3. Register purchase.\n4. Print earnings.\n5. List bidders.\n6. List paintings.\n7. Print most popular painting.\n8. Remove unwanted paintings.\n9. Quit application.\nChoice: ";
		cin >> opt;
		switch (opt) {
			case 1:
				registerPainting();
				break;
			case 2:
				registerBidder();
				break;
			case 3:
				registerPurchase();
				break;
			case 4:
				printEarnings();
				break;
			case 5:
				listBidders();
				break;
			case 6:
				listPaintings();
				break;
			case 7:
				printPopular();
				break;
			case 8:
				removeUnwanted();
				break;
			case 9:
				break;
			default:
				cout << "Operation does not exist.\n";
		}
	} while (opt != 9);
	return 0;
}
