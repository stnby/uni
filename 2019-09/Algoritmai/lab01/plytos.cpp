#include <iostream>
#include <stdio.h>

using namespace std;

struct paletes {
	long vnt;
	float kaina;
	paletes *kita;
	paletes *ankstesne;
};

paletes *pirma = NULL;
paletes *paskutine = NULL;

static long limitas = 150000;
long laisva = 150000;

void priemimas() {
	long vnt;
	float kaina;
	cout << "[?] Vienetų skaičius: ";
	cin >> vnt;
	cout << "[?] Vieneto kaina: ";
	cin >> kaina;
	if (laisva - vnt < 0) {
		cout << "[!] Šis kiekis plytų netelpa į sandėlį!\n";
		return;
	}
	laisva -= vnt;
	paletes *palete = new paletes;
	palete->vnt = vnt;
	palete->kaina = kaina;
	palete->ankstesne = NULL;
	palete->kita = pirma;
	if (pirma == NULL) {
		paskutine = palete;
	} else {
		pirma->ankstesne = palete;
	}
	pirma = palete;
}

void uzsakymas() {
	long vnt;
	float kaina = 0;
	cout << "[?] Vienetų skaičius: ";
	cin >> vnt;
	if (limitas - laisva < vnt) {
		cout << "[!] Sandelyje nėra tiek prekių!\n";
		return;
	}
	laisva += vnt;
	paletes *dabartine = paskutine;
	while (vnt > 0) {
		if (vnt - dabartine->vnt >= 0) {
			vnt -= dabartine->vnt;
			kaina += dabartine->kaina * dabartine->vnt * 1.1;
			paletes *trinama = dabartine;
			dabartine = dabartine->ankstesne;
			paskutine = dabartine;
			printf("[+] Triname: %d\n", trinama->vnt);
			delete trinama;
		}
		else {
			printf("[+] Nuimame: %d\n", vnt);
			dabartine->vnt -= vnt;
			kaina += dabartine->kaina * vnt * 1.1;
			vnt = 0;
		}
	}
	printf("[+] Kaina: %.2f€\n", kaina);
}

void inventorius() {
	paletes *dabartine = paskutine;
	cout << "[+] Inventorius:\n";
	while (dabartine != NULL) {
		printf("    %ld vnt po %.2f€\n", dabartine->vnt, dabartine->kaina);
		dabartine = dabartine->ankstesne;
	}
}

void isvalyti() {
    paletes *dabartine = paskutine;
    paletes *trinama;
    while (dabartine != NULL) {
        trinama = dabartine;
        dabartine = dabartine->ankstesne;
        printf("[+] Triname: %d\n", trinama->vnt);
        delete trinama;
    }
    pirma = NULL;
    paskutine = NULL;
    laisva = limitas;
}

void menu() {
	int opt;
	cout << "[+] Pasirinkite norimą veiksmą.\n";
	do {
		cout << "\n[1] Priimti plytas.\n[2] Vykdyti užsakymą.\n[3] Rodyti plytų partijų likutį.\n[4] Pašalinti visas plytas iš parduotuvės.\n[5] Uždaryti programą.\n\n[?] Pasirinkimas: ";
		cin >> opt;
		switch (opt) {
			case 1:
				priemimas();
				break;
			case 2:
				uzsakymas();
				break;
			case 3:
				inventorius();
				break;
			case 4:
				isvalyti();
				break;
			case 5:
				break;
			default:
				cout << "[!] Neegzistuojanti operacija!\n";
		}
	} while (opt != 5);
}

int main() {
	menu();
	return 0;
}
